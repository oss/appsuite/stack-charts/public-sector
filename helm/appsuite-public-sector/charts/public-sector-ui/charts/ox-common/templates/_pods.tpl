{{/*
Creates a list of image pull secrets and appends global.imagePullSecrets in case those are set
*/}}
{{- define "ox-common.pods.imagePullSecrets" -}}
{{- $imagePullSecrets := .imagePullSecrets -}}
{{- if .global.Values.global -}}
{{- if .global.Values.global.imagePullSecrets -}}
{{- $imagePullSecrets = concat $imagePullSecrets .global.Values.global.imagePullSecrets -}}
{{- end -}}
{{- end -}}
{{ toYaml $imagePullSecrets }}
{{- end -}}

{{/*
Generates basic attributes of a podspec to be used either in a Deployment, StatefulSet or Job (or anywhere else you need a Pod)
From the .podRoot it pulls in: serviceAccountName, podSecurityContext or securityContext, nodeSelector, affinity and tolerations. 

Example: 
spec: 
  {{ include "ox-common.pods.podSpec" "podRoot" .Values "context" . "global" $ | nintend 2}}
*/}}
{{- define "ox-common.pods.podSpec" -}}
{{- $podRoot := .podRoot -}}
{{- $global := .global -}}
imagePullSecrets: {{ include "ox-common.pods.imagePullSecrets" (dict "imagePullSecrets" $podRoot.imagePullSecrets "global" $global ) | nindent 2 }}
serviceAccountName: {{ $podRoot.serviceAccountName | default "default" }}
{{- if $podRoot.podSecurityContext }}
securityContext: {{ toYaml $podRoot.podSecurityContext | nindent 2 }}
{{- end }}
nodeSelector: {{ toYaml $podRoot.nodeSelector | nindent 2 }}
affinity: {{ toYaml $podRoot.affinity | nindent 2 }}
tolerations: {{ toYaml $podRoot.tolerations | nindent 2 }}
{{- with $podRoot.extraPodSpec }}
{{ . | toYaml }}
{{- end }}
{{- end -}}

{{/*
When configmaps or secrets change, a pod is usually not automatically restarted (Since it might react to new file content dynamically). 
If it needs a restart a common trick is to write a checksum of a rendered template into an annotation. If the template output changes, 
the checksum and therefore the annotation changes forcing kubernetes to restart the container. 

You pass a list of template names to this function, and it generates the annotations.

Example: 

annotations:
  {{ include "ox-common.pods.checksum" (list "configmap.yaml" "secret.yaml" "other-secret.yaml") | nindent 2 }}
*/}}
{{- define "ox-common.pods.checksum" -}}
{{- $context := .context -}}
{{- $global := .global -}}
{{- range .templates }}
checksum/{{ . }}: {{ include (print $global.Template.BasePath "/" . ) $context | sha256sum | trunc 10 | quote }}
{{- end -}}
{{- end -}}

