{{/*
To dump any variable or value like:
  {{- $myList := (list "a" 1 "b" (dict "c" nil)) }}
just call the named template anywhere in your template, e.g.
  {{- template "ox-common.debug.dump_var" $myList }}

This will generate the following output:
    The JSON output of the dumped var is:
    [
      "a",
      1,
      "b",
      {
        "c": null
      }
    ]
*/}}
{{- define "ox-common.debug.dump_var" -}}
{{- . | mustToPrettyJson | printf "\nThe JSON output of the dumped var is: \n%s" | fail }}
{{- end -}}
