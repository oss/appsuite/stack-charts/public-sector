{{/*
Expand the name of the chart.
*/}}
{{- define "switchboard.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "switchboard.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "switchboard.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "switchboard.labels" -}}
helm.sh/chart: {{ include "switchboard.chart" . }}
{{ include "switchboard.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "switchboard.selectorLabels" -}}
app.kubernetes.io/name: {{ include "switchboard.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "switchboard.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "switchboard.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}

{{- define "switchboard.redisHost" -}}
{{- if .Values.redis.host -}}
{{- .Values.redis.host -}}
{{- else -}}
{{- printf "%s-%s" .Release.Name "switchboard-redis" | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}

{{- define "switchboard.appsuiteSecret" -}}
{{- if .Values.overrides.appsuiteSecret -}}
{{- .Values.overrides.appsuiteSecret -}}
{{- else -}}
{{- printf "%s-%s" .Release.Name "appsuite" | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}

{{- define "switchboard.jwtSecret" -}}
{{- if .Values.overrides.jwtSecret -}}
{{- .Values.overrides.jwtSecret -}}
{{- else -}}
{{- printf "%s-%s" .Release.Name "jwt" | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}

{{- define "switchboard.redisSecret" -}}
{{- if .Values.overrides.redisSecret -}}
{{- .Values.overrides.redisSecret -}}
{{- else -}}
{{- printf "%s-%s" .Release.Name "redis-secret" | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
