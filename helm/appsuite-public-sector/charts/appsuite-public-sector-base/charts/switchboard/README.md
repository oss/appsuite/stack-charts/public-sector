# Switchboard Helm Chart

Switchboard is a service complementing an App Suite stack providing a central point to integrate additional services like e.g. presence.
The service provides and manages the websocket connections directly with App Suite UI clients. The client which will be authenticated at an existing App Suite MW via the acquire/redeemToken API

## Introduction

The chart includes the following components:

* Deployment and Service of `switchboard`
* Ingress to access the API

Requirements:

* [Redis](#redis)
* [App Suite](#app-suite)

All configuration values can be found in [values.yaml](./helm/switchboard/values.yaml).

Before the switchboard service can be deployed all dependencies should be up and running either as Kubernetes services or external services.

## Requirements

This section will provide any missing details for specific requirements.

### App Suite

In order to authenticate the deployments with App Suite, the
[redeemToken API](https://documentation.open-xchange.com/components/middleware/config/7.10.4/#mode=features&feature=Tokenlogin) needs to be configured.
In addition a secret must be created in `/opt/open-xchange/etc/tokenlogin-secrets`.
This secret must be available to the switchboard pod.

To enable the UI part of switchboard, these settings must be present for the user:

```yaml
    /opt/open-xchange/etc/settings/switchboard.properties:
      io.ox/switchboard//host: https://your.switchboard.service.deployment/

    /opt/open-xchange/etc/switchboard.properties:
      com.openexchange.capability.switchboard: "true"
```

## Configuration

| Parameter                                    | Description                                                                                  | Default                                                         |
| -------------------------------------------- | -------------------------------------------------------------------------------------------- | --------------------------------------------------------------- |
| `image.repository`                           | The image to be used for the deployment                                                      | `registry.open-xchange.com/core/switchboard`                    |
| `image.pullPolicy`                           | The imagePullPolicy for the deployment                                                       | `IfNotPresent`                                                  |
| `image.tag`                                  | The image tag, defaults to app version                                                       | `""`                                                            |
| `hostname`                                   | hostname for the switchboard deployment                                                      | `""`                                                            |
| `origins`                                    | Allowed origins for CORS                                                                     | `*`                                                             |
| `logLevel`                                   | specify log level for service                                                                | `"info"`                                                        |
| `logJson`                                    | log in JSON format                                                                           | `false`                                                         |
| `appsuite.apiSecret`                         | secret to get a session using token login                                                    | `""`                                                            |
| `appsuite.webhookSecret`                     | shared secret to secure the webhook                                                          | `""`                                                            |
| `appsuite.signatureSecret`                   | shared secret to verify webhook call signatures                                              | `""`                                                            |
| `appsuiteSecret.enabled`                     | Generate App Suite related secret, see `overrides.appsuiteSecret`                            | `true`                                                          |
| `jwtSecret.enabled`                          | Generate shared jwt secret, see `overrides.jwtSecret`                                        | `true`                                                          |
| `ingress.enabled`                            | Generate ingress resource                                                                    | `false`                                                         |
| `ingress.annotations`                        | Map of key-value pairs that will be added as annotations to the ingress resource             | `{}`                                                            |
| `overrides.name`                             | Name of the chart                                                                            | `"switchboard"`                                                 |
| `overrides.fullname`                         | Full name of the chart installation                                                          | `"RELEASE-NAME-switchboard"`                                    |
| `overrides.appsuiteSecret`                   | Prefix of the appsuite secret                                                                | `"RELEASE-NAME-appsuite"`                                       |
| `overrides.jwtSecret`                        | Prefix of the jwt secret                                                                     | `"RELEASE-NAME-jwt"`                                            |
| `redis.hosts`                                | Redis hosts as list                                                                          | `["localhost:6379"]`                                            |
| `redis.mode`                                 | Redis mode (standalone, sentinel or cluster)                                                 | `"standalone"`                                                  |
| `redis.db`                                   | Redis DB, e.g. `"1"`                                                                         | `0`                                                             |
| `redis.sentinelMasterId`                     | Name of the `sentinel` masterSet                                                             | `"mymaster"`                                                    |
| `redis.auth.enabled`                         | Generate redis auth secret                                                                   | `false`                                                         |
| `redis.auth.username`                        | Redis username                                                                               | `""`                                                            |
| `redis.auth.password`                        | Redis password                                                                               | `""`                                                            |
| `overrides.redisSecret`                      | Prefix of the redis password secret                                                          | `""`                                                            |
| `jwt.sharedSecret`                           | Shared secret for JWT authentication                                                         | `""`                                                            |
| `jwt.tokenExpiration`                        | Expiration time for JWT tokens                                                               | `"1h"`                                                          |
| `jwt.serviceCapabilities`                    | Komma separated list with capabilities to add to the token                                   | `"switchboard,openai,zoom,jitsi"`                               |
