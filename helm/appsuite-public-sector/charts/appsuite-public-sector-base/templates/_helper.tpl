{{/*
Defines the gateway name
*/}}
{{- define "appsuite.ingressGateway" -}}
{{- if not (((.Values).istio).ingressGateway).name -}}
{{- printf "%s-%s" (include "ox-common.names.fullname" .) "gateway" | quote }}
{{- else -}}
{{- .Values.istio.ingressGateway.name | quote }}
{{- end -}}
{{- end -}}

{{/*
Return the TLS secret name
*/}}
{{- define "appsuite.secretTLSName" -}}
{{- if ((((.Values).istio).ingressGateway).tls).existingSecret -}}
{{- printf "%s" .Values.istio.ingressGateway.tls.existingSecret | quote -}}
{{- else -}}
{{- printf "%s-%s" (include "ox-common.names.fullname" .) "gateway-tls-secret" | quote -}}
{{- end -}}
{{- end -}}

{{/*
Return true if a secret object should be created for TLS
*/}}
{{- define "appsuite.createTLSSecret" -}}
{{- if and (((((.Values).istio).ingressGateway).tls).enabled) (not ((((.Values).istio).ingressGateway).tls).existingSecret) }}
{{- true -}}
{{- end -}}
{{- end -}}

{{/*
Merges a list with the values of a map to provide an alternative to the list style value that can use map style inheritance.

Usage:
{{- $hosts := (include "appsuite.mergeListWithMap" (dict "list" .Values.istio.ingressGateway.hosts "map" .Values.istio.ingressGateway.overridableHosts) | fromYaml) -}}
*/}}
{{- define "appsuite.mergeListWithMap" -}}
{{- $list := .list -}}
{{- $map := .map -}}
{{- $result := list -}}
{{- range $list -}}
{{-   $result = append $result . -}}
{{- end -}}
{{- range $key, $value := $map -}}
{{-   if $value -}}
{{-     $result = append $result $value -}}
{{-   end -}}
{{- end -}}
{{- (dict "Result" $result) | toYaml -}}
{{- end -}}