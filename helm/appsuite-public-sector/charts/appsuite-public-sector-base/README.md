# AppSuite Stack Chart

This stack chart is a "full stack deployment" of App Suite. It has Istio support
built in.

# Istio

## Appsuite root path

This chart allows to configure the path under which App Suite becomes available.
By default the path is `/appsuite`.

This can get configured with the following snippet in your `values.yaml`:

```
global:
    appsuite:
        appRoot: "/appsuite"
```

`appRoot` must start with a slash, but must not end with a slash.

## Adding additional routes

It is possible to add additional services to the VirtualHost provided by App Suite.
The additional service needs to be located in the same namespace as App Suite itself.

To rewrite an exact path match to another path use the following example:

```
appsuite:
    istio:
        virtualServices:
            appsuite:
                extraRoutes:
                    - name: name-of-extra-route
                      matchExact:
                          - "/api/new-service"
                      rewrite: "/"
                      destinationHost: "new-service"
```

To rewrite a path prefix to another path use the following example:

```
appsuite:
    istio:
        virtualServices:
            appsuite:
                extraRoutes:
                    - name: name-of-extra-route
                      matchPrefix:
                          - "/api/new-service"
                      rewrite: "/"
                      destinationHost: "new-service"
```

To rewrite the path based on a regular expressione use the following example:

```
appsuite:
    istio:
        virtualServices:
            appsuite:
                extraRoutes:
                    - name: name-of-extra-route
                      matchRegex:
                          - "^/new-service/(.*)$"
                      rewriteMatchRegex: "^/new-service/(.*)/api/(.*)$"
                      rewriteRegex: "/\1/\2"
                      destinationHost: "new-service"
```

## Security headers

To enable security headers to configure referrer policy and transport security:
```
appsuite:
    istio:
        securityHeaders:
            enabled: true
```

# Optional dependencies

## Redis

By default, `core-ui-middleware` is configured to start on pod. When you plan to
scale it to a higher value, you will need `Redis`. This allows all `core-ui-middleware`
pods to communicate and synchronize with each other.

The following configuration snippet can be used:

```
appsuite:
  core-ui-middleware:
    redis:
      enabled: true
      host: appsuite-redis-master

redis:
  enabled: true
  architecture: standalone
  auth:
    enabled: false
  master:
    persistence:
      enabled: false
```

For this usecase its recommened not to scale `Redis` itself, as this might slow
down `core-ui-middleware` due to additional overhead.
